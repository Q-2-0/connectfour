from Pieces import Piece
from Winner import Winner
from BoardEvents import BoardEvents
from Broadcaster import Broadcaster


class Board(Broadcaster):

    def _cell_direction(self, idx1, idx2, horizontal):
        if horizontal:
            return self.columns[idx1][idx2]
        else:
            return self.columns[idx2][idx1]

    def _cell_direction_diagonal(self, idx1, idx2, ascending):
        if ascending:
            return self.columns[idx1][idx2]
        else:
            return self.columns[self.num_columns_rows-1-idx1][idx2]

    def _winner_by_current_color(self):
        if self.current_color == Piece.YELLOW:
            return Winner.YELLOW
        elif self.current_color == Piece.RED:
            return Winner.RED
        else:
            return Winner.NOT_YET

    def _check_winner_diagonal(self, ascending):
        for i in range(self.num_columns_rows-self.count_to_win):
            for j in range(self.num_columns_rows-self.count_to_win):
                count_pieces = 0
                for k in range(self.count_to_win):
                    if self._cell_direction_diagonal(i+k, j+k, ascending)\
                            is self.current_color:
                        count_pieces = count_pieces + 1
                    else:
                        break
                if count_pieces == self.count_to_win:
                    return self._winner_by_current_color()
        return Winner.NOT_YET

    def _check_winner_horizontal_vertical(self, horizontal):
        for i in range(self.num_columns_rows):
            count_pieces = 0
            for j in range(self.num_columns_rows):
                if self._cell_direction(j, i, horizontal)\
                        is self.current_color:
                    count_pieces = count_pieces + 1
                    if count_pieces == self.count_to_win:
                        self.winner = self._winner_by_current_color()
                else:
                    count_pieces = 0
        return self.winner

    def _check_winner(self):
        current_winner = self._check_winner_horizontal_vertical(True)
        if current_winner is Winner.NOT_YET:
            current_winner = self._check_winner_horizontal_vertical(False)
        if current_winner is Winner.NOT_YET:
            current_winner = self._check_winner_diagonal(True)
        if current_winner is Winner.NOT_YET:
            current_winner = self._check_winner_diagonal(False)

        return current_winner

    def _toggle_color(self):
        if self.current_color is Piece.YELLOW:
            self.current_color = Piece.RED
        else:
            self.current_color = Piece.YELLOW
        self._send_changed_color(self.current_color)

    def column_full(self, col_idx):
        return self.columns[col_idx][self.num_columns_rows-1] != Piece.EMPTY

    def throw_into_column(self, col_idx):
        if (col_idx < 0) or (col_idx > self.num_columns_rows-1) \
                or self.column_full(col_idx):
            return False
        else:
            for i in range(self.num_columns_rows):
                if self.columns[col_idx][i] is Piece.EMPTY:
                    self.columns[col_idx][i] = self.current_color
                    self._send_new_piece(col_idx, i, self.current_color)
                    break
            self.winner = self._check_winner()
            if self.winner is Winner.NOT_YET:
                self._toggle_color()
            else:
                self._send_new_winner()

            return True

    def _send_new_winner(self):
        for subscriber in self._subscriber:
            subscriber.on_winner(self.winner)

    def _send_new_piece(self, x: int, y: int, p: Piece):
        for subscriber in self._subscriber:
            subscriber.on_change_cell(x, y, p)

    def _send_changed_color(self, p: Piece):
        for subscriber in self._subscriber:
            subscriber.on_change_color(p)

    def subscribe(self, listener: BoardEvents):
        self._subscriber.append(listener)
        self._send_changed_color(self.current_color)

    def __init__(self, count_rows, count_win):
        self.num_columns_rows = count_rows
        self.count_to_win = count_win
        self._subscriber = []

        self.columns = []
        self.current_color = Piece.YELLOW
        self.winner = Winner.NOT_YET

        for i in range(self.num_columns_rows):
            self.columns.append([])
            j = 0
            while j < self.num_columns_rows:
                self.columns[i].append(Piece.EMPTY)
                j = j + 1
