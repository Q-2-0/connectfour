from Pieces import Piece
from Winner import Winner


class BoardEvents:

    def on_change_color(self, p: Piece):
        """ informing about new color """
        pass

    def on_change_cell(self, x: int, y: int, p: Piece):
        """ which cell changed """
        pass

    def on_winner(self, w: Winner):
        """ on winner calculated """
        pass
