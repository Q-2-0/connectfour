from BoardEvents import BoardEvents


class Broadcaster:

    def subscribe(self, listener: BoardEvents):
        """ subscribe to events happening on the board """
        pass

    def throw_into_column(self, column_index: int):
        """ tell where we put a piece in """
        pass
