from enum import Enum


class Piece(Enum):
    EMPTY = 0
    RED = 1
    YELLOW = 2

    def __str__(self):
        if self is self.YELLOW:
            return 'GELB'
        elif self is self.RED:
            return 'ROT'
        else:
            return 'LEER'
