from main_ui import ConnectFourUI
from Board import Board

number_of_columns = 7

board = Board(number_of_columns, 4)
ui = ConnectFourUI(number_of_columns, board)
ui.show()
