from tkinter import Frame
from tkinter import Tk
from tkinter import Label
from tkinter import PhotoImage
from tkinter import Button
from tkinter import messagebox
from Pieces import Piece
from Winner import Winner
from functools import partial
from BoardEvents import BoardEvents
from Broadcaster import Broadcaster


class ConnectFourUI(BoardEvents):

    _title = '4 gewinnt'
    _dimensions = '745x890'

    def on_change_color(self, p: Piece):
        self.info_label.configure(text=f"{p} ist am Zug")

    def on_change_cell(self, x: int, y: int, p: Piece):
        # get the right image file
        if p is Piece.YELLOW:
            new_label = self._yellow_image
        elif p is Piece.RED:
            new_label = self._red_image
        else:
            new_label = self._empty_image
        self._columns[y][x].configure(image=new_label)

    def on_winner(self, w: Winner):
        # give the ui the chance to update other areas
        # before the winner dialog box is opened.
        self._main_window.update_idletasks()

        # show the winner box
        messagebox.showinfo(title="Gewinner",
                            message=f"And the winner is: {w}")

    def throw_into(self, column_index):
        print(f"In Schacht {column_index+1} geworfen.")
        self._broadcaster.throw_into_column(column_index)

    def _draw_information_panel(self):
        self.info_pane = Frame(self._main_window, bg='white', height=100)
        self.info_label = Label(self.info_pane, text='xxx')
        self.info_label.pack()
        self.info_pane.pack(side='top', fill='x', expand=True)

    def _draw_interactive_board(self, count_columns):
        self.board_pane = Frame(self._main_window, bg='white')

        for i in range(count_columns):
            my_button = Button(self.board_pane,
                               text="THROW",
                               command=partial(self.throw_into, i))
            my_button.grid(row=0, column=i)

        self._empty_image = PhotoImage(file="images/empty.png")
        self._red_image = PhotoImage(file="images/red.png")
        self._yellow_image = PhotoImage(file="images/yellow.png")

        self._columns = []
        for i in range(count_columns):
            self._columns.append([])
            for j in range(count_columns):
                my_label = Label(self.board_pane, image=self._empty_image)
                my_label.grid(row=count_columns-i+1, column=j)
                self._columns[i].append(my_label)

        self.board_pane.pack(fill='both', expand=True)

    def _configure_main_window(self):
        self._main_window.title(self._title)
        self._main_window.geometry(self._dimensions)
        self._main_window.resizable(0, 0)

    def show(self):
        self._main_window.mainloop()

    def _draw_board(self):
        self._draw_information_panel()
        self._draw_interactive_board(self._number_of_columns)

    def __init__(self, number_columns: int, broadcaster: Broadcaster):
        self._number_of_columns = number_columns
        self._main_window = Tk()
        self._configure_main_window()
        self._draw_board()

        self._broadcaster = broadcaster
        self._broadcaster.subscribe(self)
