from Board import Board
from Pieces import Piece
from Winner import Winner


def test_initial_board():
    my_board_1 = Board(7, 4)

    for i in range(7):
        for j in range(7):
            assert my_board_1.columns[i][j] == Piece.EMPTY

    assert my_board_1.current_color is Piece.YELLOW


def test_fill_one_column():
    my_board_2 = Board(7, 4)
    my_board_2.throw_into_column(0)
    assert my_board_2.columns[0][0] is Piece.YELLOW
    assert my_board_2.current_color is Piece.RED


def test_fill_four_columns_and_win():
    my_board_3 = Board(7, 4)
    assert my_board_3.winner is Winner.NOT_YET
    my_board_3.throw_into_column(0)
    assert my_board_3.winner is Winner.NOT_YET
    my_board_3.throw_into_column(0)
    assert my_board_3.winner is Winner.NOT_YET
    my_board_3.throw_into_column(1)
    assert my_board_3.winner is Winner.NOT_YET
    my_board_3.throw_into_column(1)
    assert my_board_3.winner is Winner.NOT_YET
    my_board_3.throw_into_column(2)
    assert my_board_3.winner is Winner.NOT_YET
    my_board_3.throw_into_column(2)
    assert my_board_3.winner is Winner.NOT_YET
    my_board_3.throw_into_column(3)

    assert my_board_3.columns[0][0] is Piece.YELLOW
    assert my_board_3.columns[1][0] is Piece.YELLOW
    assert my_board_3.columns[2][0] is Piece.YELLOW
    assert my_board_3.columns[3][0] is Piece.YELLOW

    assert my_board_3.columns[0][1] is Piece.RED
    assert my_board_3.columns[1][1] is Piece.RED
    assert my_board_3.columns[2][1] is Piece.RED

    assert my_board_3.winner is Winner.YELLOW


def test_fill_four_columns_and_win_upper_row():
    my_board_4 = Board(7, 4)
    # yellow
    my_board_4.throw_into_column(0)
    # red
    my_board_4.throw_into_column(1)
    # yellow
    my_board_4.throw_into_column(2)
    # red
    my_board_4.throw_into_column(3)
    # yellow
    my_board_4.throw_into_column(4)
    # red
    my_board_4.throw_into_column(5)
    # yellow
    my_board_4.throw_into_column(6)

    my_board_4.throw_into_column(0)
    my_board_4.throw_into_column(0)
    my_board_4.throw_into_column(1)
    my_board_4.throw_into_column(1)
    my_board_4.throw_into_column(2)
    my_board_4.throw_into_column(2)
    my_board_4.throw_into_column(3)

    assert my_board_4.winner is Winner.RED


def test_diagonal():
    my_board_5 = Board(7, 4)

    # | | | |Y|
    # | |R|Y|R|
    # | |Y|R|Y|
    # |Y|R|Y|R|

    # row 1
    my_board_5.throw_into_column(0)
    my_board_5.throw_into_column(1)
    my_board_5.throw_into_column(2)
    my_board_5.throw_into_column(3)

    # row 2
    my_board_5.throw_into_column(1)
    my_board_5.throw_into_column(2)
    my_board_5.throw_into_column(3)

    # row 3
    my_board_5.throw_into_column(1)
    my_board_5.throw_into_column(2)
    my_board_5.throw_into_column(3)

    # row 4
    my_board_5.throw_into_column(3)

    assert my_board_5.winner is Winner.YELLOW


def test_bridge():
    my_board_6 = Board(7, 4)

    my_board_6.throw_into_column(0)
    my_board_6.throw_into_column(1)
    my_board_6.throw_into_column(2)
    my_board_6.throw_into_column(3)
    my_board_6.throw_into_column(4)
    my_board_6.throw_into_column(5)
    my_board_6.throw_into_column(6)
    my_board_6.throw_into_column(6)
    my_board_6.throw_into_column(0)
    my_board_6.throw_into_column(0)
    my_board_6.throw_into_column(1)
    my_board_6.throw_into_column(0)

    # No winner please
    my_board_6.throw_into_column(2)

    assert my_board_6.winner is Winner.NOT_YET


def test_diagonal_descending():
    my_board_7 = Board(7, 4)

    my_board_7.throw_into_column(6)
    my_board_7.throw_into_column(5)
    my_board_7.throw_into_column(5)
    my_board_7.throw_into_column(4)
    my_board_7.throw_into_column(3)
    my_board_7.throw_into_column(4)
    my_board_7.throw_into_column(4)
    my_board_7.throw_into_column(3)
    my_board_7.throw_into_column(2)
    my_board_7.throw_into_column(3)
    my_board_7.throw_into_column(3)

    assert my_board_7.winner is Winner.YELLOW
